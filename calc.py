# X-Serv-13.6-Calculadora

def suma(a: float, b: float) -> float:
	return a+b
def resta(a: float, b: float) -> float:
	return a-b

#Main
print("Resultado suma 1 + 2:", suma(1,2))
print("Resultado suma 3 + 4:", suma(3,4))
print("Resultado resta 6 - 5:", resta(6,5))
print("Resultado resta 8 - 7:", resta(8,7))
